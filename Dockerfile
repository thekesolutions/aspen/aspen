FROM registry.gitlab.com/thekesolutions/aspen/base

COPY run.sh /run.sh

CMD "/run.sh"
